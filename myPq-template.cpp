/* myPq-template.cpp
 *
 * Rev	Date				Who				Comment
 * 1.0	2016-03-24	A.McGrath	Creation
 *
 * To compile: $ g++ -std=c++11 -o myPq-template myPq_template.cpp
 * To run: ./myPq-template
 */ 
#include <string>
#include <queue>
#include <iostream>
#include <thread>
#include <mutex>
#include <errno.h>
#include "myPq-template.h"


using namespace std;  


#define ZERO	0
#define LENGTH	5


int main()
{
	myPq<struct data> fifo;


	cout << endl << "Fifo with priority template test program by A.McGrath" << endl;


	// Initialise data to be pushed onto stack.
    data x = { 0x00, "hello"};
	data y = { 0xFF, "there"};
	data z = { 0x88, "everyone"};
	data a = { 0x11, "smiley"};
	data b = { 0x11, "happy"};
    data c = { 0x11, "people"};


    cout << endl << "Test push()" << endl;
    cout << "===========" << endl;
	if(fifo.push(x) != ZERO)
		cout << "Error: push x not successful at this time" << endl;
	if(fifo.push(y) != ZERO)
		cout << "Error: push y not successful at this time" << endl;
	if(fifo.push(z) != ZERO)
		cout << "Error: push z not successful at this time" << endl;
	if(fifo.push(a) != ZERO)
		cout << "Error: push a not successful at this time" << endl;
	if(fifo.push(b) != ZERO)
		cout << "Error: push b not successful at this time" << endl;
	if(fifo.push(c) != ZERO)
		cout << "Error: push c not successful at this time" << endl;


	cout << endl << "Test pop()" << endl;
    cout << "==========" << endl;
	cout << endl;
	cout << "Priority\tString" << endl;
	cout << "--------\t------" << endl;

	while(!fifo.empty())
	{
	  data f = fifo.top();
	  cout << (int)f.priority << "\t\t" << f.sdata << endl;
	  fifo.pop();
	}


	// Push some more data items
	if(fifo.push(x) != ZERO)
		cout << "Error: push x not successful at this time" << endl;
	if(fifo.push(y) != ZERO)
		cout << "Error: push y not successful at this time" << endl;
	if(fifo.push(c) != ZERO)
		cout << "Error: push z not successful at this time" << endl;


	cout << endl << "Test pop_try()" << endl;
    cout << "==============" << endl;
	cout << endl;
	cout << "Priority\tString" << endl;
	cout << "--------\t------" << endl;

	for(int count = 0; count < LENGTH; count++)
	{
		data f = fifo.top();
	  	cout << (int)f.priority << "\t\t" << f.sdata << endl;
	  	int ret = fifo.pop_try();
	  	if(ret != 0)
	  		cout << "Error: No data available" << endl;
	}


	return 0;
}
