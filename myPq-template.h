/* myPq-template.h
 *
 * Rev	Date				Who				Comment
 * 1.0	2016-03-24	A.McGrath	Creation
 *
 */
#include <string>
#include <queue>
#include <iostream>
#include <thread>
#include <mutex>
extern "C" 
{
#include <errno.h>
#include <unistd.h> 	// usleep()
}


using namespace std;  


#define ZERO			0
#define LENGTH			5
#define MSECS 			100
#define WAIT_ATTEMPTS	5


//
// Data structure that includes a priority
//
struct data
{
  unsigned char priority; // 0 = lowest, 255 = highest
  string sdata;
  bool operator<(const data& rhs) const
  {
	return priority < rhs.priority;
  }
};


//
// Inherit myPq from priority_queue and add the required functionality
//
template<typename T>
class myPq : public priority_queue<T> 
{
private:
	int count = ZERO;
	int max_size = LENGTH;
	unsigned int microsecs = MSECS;
	bool data_available = false;
	
	/*
	 * Keep track of number of elements added to queue - increment
	 */
	void count_inc()
	{
		myPq::count++;
	}

	/*
	 * Keep track of number of elements added to queue - decrement
	 */
	void count_dec()
	{
		if(myPq::count > 0)
		{
			myPq::count--;
		}
	}

	// Want mutex to be locked the minimum amount of time
	std::mutex m; 

public:
    // constructor 
    myPq(){}

    // destructor 
    ~myPq(){}

    /* push */
    int push(T a)
    {
		m.lock();
		if(myPq::count < LENGTH)
		{
			// Push entry onto the queue
			priority_queue<T>::push(a);
			m.unlock();
			myPq::count_inc();
			data_available = true;
			return ZERO;
		}
		else
		{
			// Queue length has been reached.
			m.unlock();
			return -EAGAIN; // try again at a later time
		}
	}

    /* pop */
    void pop(void)
    {
		int ret = 1;
		m.lock();
		if(priority_queue<T>::empty())
		{
			// If queue empty, unlock mutex
			m.unlock();
			// sleep for a while, check for available data
			while((myPq::data_available == false) && (ret != 0))
			{
			  usleep(myPq::microsecs);
			  ret = pop_try();
			}
			// There may be a more elegant way to achieve this objective e.g a signal being 
			// raised in myPq::push() and ending interruptible sleep in myPq::pop() thus   
			// causing program execution to resume.
			// In the embedded microcontroller world an interrupt service routine could be
			// used to signal that priority fifo data is available.
		}
		else
		{
			// Pop next highest priority data off the queue
			priority_queue<T>::pop();
			m.unlock();
			myPq::count_dec();
		}
		myPq::data_available = false;
    }

    /* pop_try */
    int pop_try(void)
    {
		m.lock();
        if(priority_queue<T>::empty())
        {
        	// If queue empty unlock mutex and return with error "No Data"
        	m.unlock();
			return -ENODATA;
        }
        else
        {
        	// otherwise, pop value, decrement counter and return ZERO
        	priority_queue<T>::pop();
			m.unlock();
			myPq::count_dec();
        	return ZERO;
        }	
    }
};

